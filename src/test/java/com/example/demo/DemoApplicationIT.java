package com.example.demo;

import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.time.ZonedDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class DemoApplicationIT {
    @Autowired
    private ToDoRepository toDoRepository;

    @Autowired
    private ToDoService toDoService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenGetOne_returnValidResponse() throws Exception {
        ToDoEntity toDoEntity = new ToDoEntity("test").completeNow();

        toDoRepository.save(toDoEntity);

        String url = "/todos/" + toDoEntity.getId().toString();

        this.mockMvc
                .perform(get(url))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.text").value(toDoEntity.getText()))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(toDoEntity.getId()))
                .andExpect(jsonPath("$.completedAt").exists());
    }

    @Test
    public void whenFindByText_returnMatchedResponse() {
        String testPattern = "text";

        ToDoEntity toDoEntity = new ToDoEntity(testPattern);

        toDoRepository.save(toDoEntity);

        var toDos = toDoService.findByTextPattern(testPattern);

        assertEquals(1, toDos.size());
        assertThat(toDos.get(0), samePropertyValuesAs(ToDoEntityToResponseMapper.map(toDoEntity)));
    }
}
