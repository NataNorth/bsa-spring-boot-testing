package com.example.demo;

import com.example.demo.controller.ToDoController;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.mockito.ArgumentMatchers;


import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.aMapWithSize;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoService.class)
public class ToDoControllerWithServiceIT {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoRepository toDoRepository;

    @Test
    void whenCompleteIdNotFound_thenThrowNotFoundException() throws Exception {
        when(toDoRepository.findById(0L)).thenReturn(Optional.empty());
        this.mockMvc.perform(put("/todos/0/complete"))
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ToDoNotFoundException));
    }

    @Test
    void whenGetOneIdNotFound_thenThrowNotFoundException() throws Exception {
        when(toDoRepository.findById(0L)).thenReturn(Optional.empty());
        this.mockMvc.perform(get("/todos/0"))
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ToDoNotFoundException));
    }

    @Test
    void whenUpsertIdNotFound_thenThrowNotFoundException() throws Exception {

        when(toDoRepository.findById(0L)).thenReturn(Optional.empty());

        ToDoSaveRequest toDoSaveRequest = new ToDoSaveRequest();
        toDoSaveRequest.id = 0L;
        toDoSaveRequest.text = "test";

        this.mockMvc.perform(post("/todos")
                .content(asJsonString(toDoSaveRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ToDoNotFoundException));
    }

    @Test
    void whenFindByTextWithText_returnValidResult() throws Exception {
        String testPattern = "test";
        var responseEntity =  new ToDoEntity(0L,"1 Test");
        responseEntity.completeNow();

        when(toDoRepository.findByTextContainingIgnoreCase(testPattern)).thenReturn(List.of(responseEntity));

        this.mockMvc
                .perform(get("/todos/text?pattern=test"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].text").value(responseEntity.getText()))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].completedAt").exists());
    }

    @Test
    void whenUpsertWithId_returnValidRequest() throws Exception {

        var responseToDo = new ToDoEntity(0L, "New toDo");
        when(toDoRepository.findById(anyLong())).thenAnswer(i -> {
            Long id = i.getArgument(0, Long.class);
            if (id.equals(responseToDo.getId())){
                return Optional.of(responseToDo);
            } else {
                return Optional.empty();
            }
        });
        when(toDoRepository.findById(0L)).thenReturn(Optional.of(responseToDo));
        when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
            ToDoEntity arg = i.getArgument(0, ToDoEntity.class);
            Long id = arg.getId();
            if(id != null) {
                if (!id.equals(responseToDo.getId()))
                    return new ToDoEntity(id, arg.getText());
                responseToDo.setText(arg.getText());
                return responseToDo;
            }
            else {
                return new ToDoEntity(5555L, arg.getText());
            }
        });

        ToDoSaveRequest toDoSaveRequest = new ToDoSaveRequest();
        toDoSaveRequest.id = 0L;
        toDoSaveRequest.text = "test";

        this.mockMvc.perform(post("/todos")
                .content(asJsonString(toDoSaveRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.text").value(toDoSaveRequest.text))
                .andExpect(jsonPath("$.id").value(toDoSaveRequest.id))
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

}
