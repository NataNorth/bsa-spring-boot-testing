package com.example.demo;

import com.example.demo.controller.ToDoController;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
public class DemoApplicationTest {
    @Autowired
    private ToDoController controller;

    @Test
    void contextLoad() {
        assertThat(controller).isNotNull();
    }
}
