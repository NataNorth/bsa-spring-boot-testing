package com.example.demo;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class ToDoServiceTest {
    private ToDoRepository toDoRepository;

    private ToDoService toDoService;

    @BeforeEach
    void setUp(){
        this.toDoRepository = mock(ToDoRepository.class);
        toDoService = new ToDoService(toDoRepository);
    }

    @Test
    void whenFindByText_returnMatchedList(){
        var responseEntity =  new ToDoEntity("Test");
        responseEntity.completeNow();

        String testPattern = "test";

        when(toDoRepository.findByTextContainingIgnoreCase(testPattern)).thenReturn(List.of(responseEntity));

        var toDos = toDoService.findByTextPattern(testPattern);

        assertEquals(1, toDos.size());
        assertThat(toDos.get(0), samePropertyValuesAs(ToDoEntityToResponseMapper.map(responseEntity)));
    }

    @Test
    void whenFindByTextWithoutText_returnAll(){
        var testToDos = new ArrayList<ToDoEntity>();
        testToDos.add(new ToDoEntity("First"));
        testToDos.add(new ToDoEntity("Second"));

        String testPattern = "";

        toDoService = mock(ToDoService.class);

        when(toDoService.getAll()).thenReturn(
                testToDos.stream()
                        .map(ToDoEntityToResponseMapper::map)
                        .collect(Collectors.toList()));

        when(toDoService.findByTextPattern(testPattern)).thenCallRealMethod();

        List<ToDoResponse> toDos = toDoService.findByTextPattern(testPattern);

        assertEquals(testToDos.size(), toDos.size());
        for(int i = 0; i < testToDos.size(); i++){
            assertThat(toDos.get(i), samePropertyValuesAs(ToDoEntityToResponseMapper.map(testToDos.get(i))));
        }
    }

    @Test
    void whenUpsertIdNotFound_thenThrowNotFoundException(){
        ToDoSaveRequest toDoSaveRequest = new ToDoSaveRequest();
        toDoSaveRequest.id = 0L;
        toDoSaveRequest.text = "Test";
        when(toDoRepository.findById(0L)).thenReturn(Optional.empty());
        assertThrows(ToDoNotFoundException.class, () -> toDoService.upsert(toDoSaveRequest));
    }

    @Test
    void whenCompleteIdNotFound_thenThrowNotFoundException() {
        Long testId = 0L;
        when(toDoRepository.findById(testId)).thenReturn(Optional.empty());
        assertThrows(ToDoNotFoundException.class, () -> toDoService.completeToDo(testId));
    }
}
